//
//  gameScreem.m
//  breakout1
//
//  Created by Xian Wu on 2/10/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "gameScreen.h"

@implementation gameScreen
@synthesize timer,paddle,ball,score,life;
CGFloat height;
bool begin;
NSArray<UIView *> *bricks;
NSString *scores,*lifes;
int scorei,lifei;
-(void)createPlayField
{
    
    lifei =2;
    scorei = 0;
    lifes = [NSString stringWithFormat:@"life: %d",lifei];
    [life setValue:lifes forKey:@"text"];
    scores = [NSString stringWithFormat:@"score: %d",scorei];
    [score setValue:scores forKey:@"text"];
    bricks = [[NSArray alloc] init];
    CGFloat borderW = 1.0f;
    begin = 0;
    CGRect rect = self.frame;
    CGSize size = rect.size;
    //CGFloat width = size.width;
    height = size.height+40;
    //height = self.frame.size.height;
    paddle = [[UIView alloc] initWithFrame:CGRectMake(20, height, 60, 10)];
    [self addSubview:paddle];
    [paddle setBackgroundColor:[UIColor whiteColor]];
    
    ball = [[UIView alloc] initWithFrame:CGRectMake(50, height-10, 10, 10)];
    ball.center = CGPointMake(paddle.center.x, paddle.center.y-10);
    [self addSubview:ball];
    [ball setBackgroundColor:[UIColor redColor]];
    CGRect rect1 = [[UIScreen mainScreen]bounds];
    CGFloat width = rect1.size.width-30;
    
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 8; j++)
        {
            UIView *ui = [[UIView alloc]initWithFrame:CGRectMake(j*width/8, height-500+20*i, width/8-borderW, 20-borderW)];
            ui.tag = 8*i+j+10;
            //NSLog(@"tag is %lu",ui.tag);
            if(i==0)[ui setBackgroundColor:[UIColor orangeColor]];
            else if(i==1)[ui setBackgroundColor:[UIColor greenColor]];
            else if(i==2)[ui setBackgroundColor:[UIColor redColor]];
            else if(i==3)[ui setBackgroundColor:[UIColor yellowColor]];
            ui.layer.borderWidth = borderW;
            ui.layer.borderColor = [UIColor blackColor].CGColor;
            ui.alpha = .6f;
            //[ui setHidden:YES];
            //[ui setLayoutMargins:<#(UIEdgeInsets)#>]
            [self addSubview:ui];
        }
        
    }
    //[[self viewWithTag:10]setBackgroundColor: [UIColor blackColor]];
    dx = 2;
    dy = 2;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for (UITouch *t in touches)
    {
        CGPoint p = [t locationInView:self];
        CGFloat px = p.x;
        //[paddle setCenter:p];
        paddle.center = CGPointMake(px, paddle.center.y);
        if(!begin){
                ball.center = CGPointMake(paddle.center.x, paddle.center.y-10);
        }
//        [paddle setFrame:CGRectMake(px, height, 60, 10)];
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesBegan:touches withEvent:event];
}

-(IBAction)startAnimation:(id)sender
{
    begin = 1;
    timer = [NSTimer scheduledTimerWithTimeInterval:.01	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    
}

-(IBAction)stopAnimation:(id)sender
{
    [timer invalidate];
}

-(void)timerEvent:(id)sender
{
    CGRect bounds = [self bounds];
    
    // NSLog(@"Timer event.");
    CGPoint p = [ball center];
    
    if ((p.x + dx) < 0)
        dx = -dx;
    
    if ((p.y + dy) < 0)
        dy = -dy;
    
    if ((p.x + dx) > bounds.size.width)
        dx = -dx;
    
    if ((p.y + dy) > bounds.size.height)
        dy = -dy;
    
    p.x += dx;
    p.y += dy;
    [ball setCenter:p];
    
    // Now check to see if we intersect with paddle.  If the movement
    // has placed the ball inside the paddle, we reverse that motion
    // in the Y direction.
    if (CGRectIntersectsRect([ball frame], [paddle frame]))
    {
        dy = -dy;
        p.y += 2*dy;
        [ball setCenter:p];
    }
    int i = 10;
    UIView *temp;
    for(; i < 42; i++){
        temp = [self viewWithTag:i];
        if(![temp isHidden])
            if (CGRectIntersectsRect([ball frame], [temp frame]))
            {
                [temp setHidden:YES];
                dy = -dy;
                p.y += 2*dy;
                [ball setCenter:p];
                scorei++;
                scores = [NSString stringWithFormat:@"score: %d",scorei];
                [score setValue:scores forKey:@"text"];
                break;
            }
    }
    if(ball.center.y>(paddle.center.y+20)){
        
        [timer invalidate];
        if(lifei > 0){
            lifei--;
            lifes = [NSString stringWithFormat:@"life: %d",lifei];
            [life setValue:lifes forKey:@"text"];
            ball.center = CGPointMake(paddle.center.x, paddle.center.y-10);
            begin = 0;
        }
        else{
        UILabel *lose = [[UILabel alloc]initWithFrame:CGRectMake(60,233,200,100)];
        [lose setBackgroundColor:[UIColor whiteColor]];
//        [lose setvalue:@"YOU LOSE" forKey:@"text"];
        [lose setText:@"YOU LOSE"];
        [lose setTextAlignment:NSTextAlignmentCenter];
        [lose setTextColor:[UIColor redColor]];
        //[lose set]
        lose.alpha = 0.7f;
        [self addSubview:lose];
        }
    }
    
}

@end