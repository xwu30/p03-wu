//
//  ViewController.m
//  breakout1
//
//  Created by Xian Wu on 2/10/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize gameS;

- (void)viewDidLoad {
    [super viewDidLoad];
    [gameS createPlayField];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
