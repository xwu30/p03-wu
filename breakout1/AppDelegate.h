//
//  AppDelegate.h
//  breakout1
//
//  Created by Xian Wu on 2/10/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

