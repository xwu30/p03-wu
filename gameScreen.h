//
//  gameScreem.h
//  breakout1
//
//  Created by Xian Wu on 2/10/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface gameScreen : UIView{
    float dx,dy;
}
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong)IBOutlet UILabel *score;
@property (nonatomic, strong)IBOutlet UILabel *life;
-(void)createPlayField;
@end
